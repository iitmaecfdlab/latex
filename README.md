# Latex Template
### A template for projects documented with latex
##### **Author**: Jatinder Pal Singh Sandhu

## Directory Sturcture
#### tex
Contains all <some>.tex files which you may or may not include in the main.tex for compilation. Main content of you project is situated in this directory.

#### style
Contains the latex class, bibliography style, and user defined style files. User need to keep all mentioned file in this folder. By default AIAA class file (new-aiaa.cls), AIAA bibliography style file (new-aiaa.bst) is provided in this folder. IITM latex class and bibliography style is also given in this folder. The **mystyle.sty** file contains user defined style where you can write your whole preamble for the document.

#### reference
In this folder you keep all the reference in bibtex format which will be cited in the tex document. If you keep the name of the file same (**cite.bib**), you do not need to change anything for variable citations in the makefile (See instruction below). In make file bibtex will be executed once, so make sure that there is atleast one citation in any .tex file, or remove bibtex execution line from the makefile.

#### img
In this folder keep all the images.

#### Out
All the temporary and PDF file will be kept in this folder. Git will not track this folder as it contains non-essential files.

#### main.tex
The main file in which you include your custom style **mystyle** and **tex** file. 

*By default all these file are included to provide user an idea how to use any new style file or tex file.*



## Instructions for creating document
1. clone this reposity using:
```markdown
git clone https://gitlab.com/iitmaecfdlab/latex.git 
```
2. Update following in **Makefile**:
   Title: ProjectTitle < not neccessary >
   Bibstyle: bibliography style kept in style folder < Change only if using different bibliography style >
  Citaions: bibtex file containing references. eg.$(RefFolder)/**cite.bib** . < can skip this step is your keeping all the references in the provide cite.bib file. >
3. Keep latex class and bibliography style file in **style/** folder. In class file, make sure the name in  **Proveideclass** function contais the **style/** prefix, so the it can found from the main.tex which is one folder up the directory sturcture.
4. Write main content in *tex/<files>.tex* and input it in main.tex file.
```
  \input{tex/<files>.tex}
```
5. Keep reference in **reference/<citefile>.bib** and call it main.tex using 
```
\bibliography{cite}
```
6. Finally create PDF using:
```
make
```
PDF will be located in **Out/** folder.
7. To create a compressed verion of current folder use:
```
make tar
```
8. To remove temporary files use:
``` 
make clean
```


## Instuction for collaboration
1. If you do not have the repository on which you want to collaborate, you need to clone it first:
```
git clone "https://gitlab.com/<AnyRepo>.git"
```
Change "AnyRepo" to particular repository you want to clone. 

If you already have the repository on your local computer, you can skip above step and instead update the existing repository using followig command:
```
git pull
```
2. If you are begining to work on this repo you will need to create a new branch from the current **master** branch. 
```
git checkout -b review
```
A new branch **review**  branch will be created and it will automatically switch to that branch.
If you returing back to this repo which already have review branch, you can just switch to that branch if not already in that particular branch:
- Check on which branch you are on:
``` 
git branch -v
```
- Switch to **review** branch if needed:
```
git checkout review
```
3. check if **review** branch and **master** branch at different state:
``` git
git diff review master 
```
If difference is found you will need to update the review the "review" branch by using:
```git
git rebase master
```
3. Make change to the file in **tex** folder. Keep track of all the changes using following command.
 - Check status of all file ``` git status``` or for less information(short) ``` git status -sb ```
 - Find the changes made to particular file ``` git diff tex/<file.tex>```
 - Find the word by word change by ``` git diff --word-diff tex/<file.tex>```
4. Once statisfied with all the changes. you can commit these changes by following command
``` 
git add .
git commit -m "Commit message"
```
5 you need to push all these changes to online repo using following command:
``` 
git push -u origin review
```

*You can perform all above collaboration task with [Gitkraken](https://www.gitkraken.com/), which is a gui helps you avoid writting all git commands in terminal*

### List of useful git commands
  - Clone an existing repository: ```git clone <online directory>```
  - Create a new local repository: ```git init```
  - changed file in your working directory: ```git status -sb```
  - Changes to tracked files: ``` git diff ``` or ```git diff --word-diff```
  - Add all the current changes to the next commit: ``` git add . ```
  - Add some changes in <file> to the next commit: ``` git add -p <file> ```
  - Commit all local changes in tracked files: ``` git commit -a ```
  - Commit previously staged changes: ```git commit -m "message"```
  - Show all commits```git log --oneline --decorate```
  - List all existing branches ``` git branch -av```
  - Switch HEAD branch ```git checkout <branch>```
  - Create a new branch on your current HEAD: ```git branch <new-branch>```
  - Delete a local branch : ``` git branch -d <branch>```
  - Download all the changes from <remote>, but do not integrate into HEAD: ``` git fetch <remote>```
  - Download changes and directly merge/integrate into HEAD: ``` git pull <remote> <branch>```
  - Publish local changes on a remote: ```git push <remote> <branch>```
  - Delete a branch on the remote: ``` git branch -dr <remote/branch>```
  - Merge <branch> into your current HEAD: ``` git merge <branch>```
  - Rebase Your current HEAD onto <branch>: ```git rebase <branch>```
