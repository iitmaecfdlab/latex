Title:=LatexDoc
Style:=style
Out:=out
Tex:=tex

#bibliography style
Bibstyle:=new-aiaa.bst
RefFolder:=reference
Bibfile:=$(Style)/$(Bibstyle)
Citations:=$(RefFolder)/cite.bib

all: main.tex
	mkdir -p $(Out)
	pdflatex -halt-on-error -output-directory=$(Out) main
	cp $(Citations) $(Out)/; cp $(Bibfile) $(Out)/
	cd $(Out); bibtex main
	pdflatex -halt-on-error -output-directory=$(Out) main
	pdflatex -halt-on-error -output-directory=$(Out) main


#clean the directory
.PHONY: clean
clean:
	rm -rf out/

# compressed version of directory
.PHONY: tar
tar:
	tar -zcvf $(Title).tar.gz $(Style)/ $(Tex)/  $(RefFolder) makefile main.tex
